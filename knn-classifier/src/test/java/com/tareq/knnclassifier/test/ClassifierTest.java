/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tareq.knnclassifier.test;

import com.tareq.knnclassifier.ChebyshevDistanceCalculator;
import com.tareq.knnclassifier.ChiSquaredDistanceCalculator;
import com.tareq.knnclassifier.CosineDistanceCalculator;
import com.tareq.knnclassifier.EuclideanDistanceCalculator;
import com.tareq.knnclassifier.FeatureVector;
import com.tareq.knnclassifier.GowerDistanceCalculator;
import com.tareq.knnclassifier.KnnClassifier;
import com.tareq.knnclassifier.ManhattanDistanceCalculator;
import java.io.File;
import java.util.logging.Logger;
import org.junit.Test;

/**
 *
 * @author tareq
 */
public class ClassifierTest {

    private static final Logger LOG = Logger.getLogger(ClassifierTest.class.getName());

    
    @Test
    public void testCalculators() {
        File file = new File("iris.csv");
        KnnClassifier classifier = new KnnClassifier(file,5);
        double[] test = {5.1, 3.5, 1.4, 0.2};
        double[] test2 = {6.7, 3.1, 5.6, 2.4};
        FeatureVector v = new FeatureVector(test);
        FeatureVector v2 = new FeatureVector(test2);
        LOG.info(classifier.classify(v2, new ManhattanDistanceCalculator()));
        LOG.info(classifier.classify(v, new ChebyshevDistanceCalculator()));
        LOG.info(classifier.classify(v2, new EuclideanDistanceCalculator()));
        LOG.info(classifier.classify(v, new CosineDistanceCalculator()));
        LOG.info(classifier.classify(v2, new ChiSquaredDistanceCalculator()));
        LOG.info(classifier.classify(v, new GowerDistanceCalculator(new double[]{0.03, 0.5, 1, 0.1})));
        LOG.info(classifier.getStats());
         classifier = new KnnClassifier(file,15);
        LOG.info(classifier.classify(v2, new ManhattanDistanceCalculator()));
        LOG.info(classifier.classify(v, new ChebyshevDistanceCalculator()));
        LOG.info(classifier.classify(v2, new EuclideanDistanceCalculator()));
        LOG.info(classifier.classify(v, new CosineDistanceCalculator()));
        LOG.info(classifier.classify(v2, new ChiSquaredDistanceCalculator()));
        LOG.info(classifier.classify(v, new GowerDistanceCalculator(new double[]{0.03, 0.5, 1, 0.1})));
        LOG.info(classifier.getStats());
        classifier.calculateGowerAccuracy(new double[]{0,0.5,1,0});
        LOG.info(classifier.getStats());
    }

}
