/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tareq.knnclassifier;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;
import java.util.List;

/**
 *
 * @author tareq
 */
public class EuclideanDistanceCalculator implements DistanceCalculator{


    @Override
    public double calculate(double[] a, double[] b) {
        double sum = 0;
        for(int i =0;i<a.length;i++){
            sum += sqrt(pow((a[0]-b[0]),2));
        }
        return sum;
    }
    
}
