/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tareq.knnclassifier;

import java.io.File;
import static java.lang.Math.floor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import static java.util.concurrent.Executors.newFixedThreadPool;
import java.util.concurrent.FutureTask;
import java.util.logging.Logger;
import java.util.stream.Stream;

/**
 *
 * @author tareq
 */
public class KnnClassifier {

    private final ExecutorService executor;
    private final int poolSize;
    private final ArrayList<FeatureVector> vectorSet;
    private final ArrayList<FeatureVector> testSet;
    private double[] maxes;
    private double[] mins;
    private double[] diffs;
    private int k;
    private HashMap<String, Double> accuracy;
    private int featureVectorLength;
    private static final Logger LOG = Logger.getLogger(KnnClassifier.class.getName());

    public KnnClassifier(File csvFile, int k) {
        this.k = k;
        ArrayList<FeatureVector> allSet = FileUtils.readFile(csvFile);
        poolSize = calculatePoolSize(allSet.size());
        executor = newFixedThreadPool(poolSize);
        normalizeVectorSet(allSet);
        int size = allSet.size();
        double testSetSize = floor(0.3 * size);
        testSet = new ArrayList();
        Random r = new Random();
        for (int i = 0; i < testSetSize; i++) {
            int random = (int) (Math.random() * (allSet.size() - 0)) + 0;
            testSet.add(allSet.remove(random));
        }
        vectorSet = allSet;
        calculateAccuracy();
    }

    private void normalizeVectorSet(ArrayList<FeatureVector> features) {
        featureVectorLength = features.get(0).getFeatures().length;
        maxes = new double[featureVectorLength];
        mins = new double[featureVectorLength];
        diffs = new double[featureVectorLength];
        findMinMax(features);
        features.forEach((v) -> {
            normalizeVector(v);
        });

    }

    private void findMinMax(ArrayList<FeatureVector> vectorSet) {
        ArrayList<FutureTask<Void>> taskList = new ArrayList();
        for (int i = 0; i < featureVectorLength; i++) {
            taskList.add((FutureTask<Void>) executor.submit(new FindMinMaxCallable(vectorSet, i)));
        }
        for (FutureTask<Void> task : taskList) {
            try {
                task.get();
            } catch (InterruptedException | ExecutionException ex) {
                LOG.severe(ex.getMessage());
            }
        }

    }

    private void normalizeVector(FeatureVector v) {
        for (int i = 0; i < featureVectorLength; i++) {
            if (v.getFeatures()[i] > maxes[i]) {
                v.getFeatures()[i] = 1;
            } else if (v.getFeatures()[i] < mins[i]) {
                v.getFeatures()[i] = 0;
            } else {
                v.getFeatures()[i] = (v.getFeatures()[i] - mins[i]) / diffs[i];
            }
        }
    }

    public String classify(FeatureVector featureVector, DistanceCalculator d) {
        normalizeVector(featureVector);
        List<FutureTask<Map<FeatureVector, Double>>> futureTaskList = new ArrayList();
        ArrayList<ArrayList<FeatureVector>> featureVectorChunks = chunks(vectorSet, poolSize);
        featureVectorChunks.forEach((chunk) -> {
            futureTaskList.add(
                    (FutureTask<Map<FeatureVector, Double>>) executor.submit(new DistanceCalculationCallable(chunk, d, featureVector)));
        });
        HashMap<FeatureVector, Double> calculationResults = new HashMap();
        futureTaskList.forEach((task) -> {
            try {
                calculationResults.putAll(task.get());
            } catch (InterruptedException | ExecutionException ex) {
                LOG.severe(ex.getMessage());
            }
        });
        Stream<Map.Entry<FeatureVector, Double>> sorted;
        sorted = calculationResults.entrySet().stream()
                .sorted(Map.Entry.comparingByValue());
        Iterator<Map.Entry<FeatureVector, Double>> distanceIterator = sorted.iterator();
        int i = 0;
        HashMap<String, Integer> frequencyMap = new HashMap();
        while (distanceIterator.hasNext() && i < k) {
            Map.Entry<FeatureVector, Double> next = distanceIterator.next();
            if (!frequencyMap.containsKey(next.getKey().getClassLabel())) {
                frequencyMap.put(next.getKey().getClassLabel(), 0);
            }
            int repetition = frequencyMap.get(next.getKey().getClassLabel());
            frequencyMap.put(next.getKey().getClassLabel(), repetition + 1);
            i++;
        }
        int maxFrequency = 0;
        String maxClass = null;
        for (String key : frequencyMap.keySet()) {
            if (frequencyMap.get(key) > maxFrequency) {
                maxFrequency = frequencyMap.get(key);
                maxClass = key;
            }
        }
        featureVector.setClassLabel(maxClass);
        return maxClass;
    }

    private int calculatePoolSize(int vectorLength) {
        int calculated = vectorLength / 10000;
        if (calculated < 1) {
            return 1;
        } else if (calculated > 10) {
            return 10;
        } else {
            return calculated;
        }
    }

    public static ArrayList<ArrayList<FeatureVector>> chunks(ArrayList<FeatureVector> bigList, int n) {
        ArrayList<ArrayList<FeatureVector>> chunks = new ArrayList<>();
        for (int i = 0; i < bigList.size(); i += n) {
            ArrayList<FeatureVector> chunk = new ArrayList(bigList.subList(i, Math.min(bigList.size(), i + n)));
            chunks.add(chunk);
        }
        return chunks;
    }

    private static final class DistanceCalculationCallable implements Callable<Map<FeatureVector, Double>> {

        private final List<FeatureVector> vectorSet;
        private final DistanceCalculator distanceCalculation;
        private final FeatureVector vector;

        public DistanceCalculationCallable(List<FeatureVector> featureVectorSet, DistanceCalculator distanceCalculation, FeatureVector vector) {

            this.vectorSet = featureVectorSet;
            this.distanceCalculation = distanceCalculation;
            this.vector = vector;
        }

        @Override
        public Map<FeatureVector, Double> call() throws Exception {
            HashMap<FeatureVector, Double> partialResult = new HashMap();
            vectorSet.forEach((feature) -> {
                partialResult.put(feature, distanceCalculation.calculate(feature.getFeatures(), vector.getFeatures()));
            });
            return partialResult;
        }

    }

    private class FindMinMaxCallable implements Callable<Void> {

        private final ArrayList<FeatureVector> features;
        private final int featureId;

        public FindMinMaxCallable(ArrayList<FeatureVector> features, int featureId) {
            this.features = features;
            this.featureId = featureId;
        }

        @Override
        public Void call() throws Exception {
            double max = Integer.MIN_VALUE;
            double min = Integer.MAX_VALUE;
            for (FeatureVector v : features) {
                double value = v.getFeatures()[featureId];
                if (value < min) {
                    min = value;
                }
                if (value > max) {
                    max = value;
                }
            }
            maxes[featureId] = max;
            mins[featureId] = min;
            diffs[featureId] = max - min;
            return null;
        }
    }

    private void calculateAccuracy() {
        int accuracyManhattan = testSet.size();
        int accuracyEuclidean = testSet.size();
        int accuracyCosine = testSet.size();
        int accuracyChiSquared = testSet.size();
        int accuracyGower = testSet.size();
        int accuracyChebyshev = testSet.size();

        DistanceCalculator manhattan = new ManhattanDistanceCalculator();
        DistanceCalculator euclidean = new EuclideanDistanceCalculator();
        DistanceCalculator cosine = new CosineDistanceCalculator();
        DistanceCalculator chiSquared = new ChiSquaredDistanceCalculator();
        DistanceCalculator gower = new GowerDistanceCalculator();
        DistanceCalculator chebyshev = new ChebyshevDistanceCalculator();
        for (FeatureVector v : testSet) {
            String classLabel = v.getClassLabel();
            classify(v, manhattan);
            if (!classLabel.equals(v.getClassLabel())) {
                accuracyManhattan--;
            }
            classify(v, euclidean);
            if (!classLabel.equals(v.getClassLabel())) {
                accuracyEuclidean--;
            }
            classify(v, cosine);
            if (!classLabel.equals(v.getClassLabel())) {
                accuracyCosine--;
            }
            classify(v, chiSquared);
            if (!classLabel.equals(v.getClassLabel())) {
                accuracyChiSquared--;
            }
            classify(v, gower);
            if (!classLabel.equals(v.getClassLabel())) {
                accuracyGower--;
            }
            classify(v, chebyshev);
            if (!classLabel.equals(v.getClassLabel())) {
                accuracyChebyshev--;
            }
            v.setClassLabel(classLabel);
        }
        accuracy = new HashMap<>();
        accuracy.put("Manhattan", (double) accuracyManhattan * 100 / (double) testSet.size());
        accuracy.put("Euclidean", (double) accuracyEuclidean * 100 / (double) testSet.size());
        accuracy.put("Cosine", (double) accuracyCosine * 100 / (double) testSet.size());
        accuracy.put("Chi Squared", (double) accuracyChiSquared * 100 / (double) testSet.size());
        accuracy.put("Gower", (double) accuracyGower * 100 / (double) testSet.size());
        accuracy.put("Chebyshev", (double) accuracyChebyshev * 100 / (double) testSet.size());

    }

    public void calculateGowerAccuracy(double[] weights) {
        GowerDistanceCalculator gower = new GowerDistanceCalculator(weights);
        int accuracyGower = testSet.size();
        for (FeatureVector v : testSet) {
            String classLabel = v.getClassLabel();
            classify(v, gower);
            if (!classLabel.equals(v.getClassLabel())) {
                accuracyGower--;
            }
            v.setClassLabel(classLabel);
        }
        accuracy.put("Gower", (double) accuracyGower / (double) testSet.size());

    }

    public String getStats() {
        StringBuilder sb = new StringBuilder();
        sb.append("size : ").append(vectorSet.size()).append("\n");
        sb.append("features :").append(featureVectorLength).append("\n");
        sb.append("feature : min : max \n");
        for (int i = 0; i < featureVectorLength; i++) {
            sb.append(i).append(" : ").append(mins[i]).append(" : ").append(maxes[i]).append("\n");
        }
        sb.append("accuracy : \n ");
        for (String string : accuracy.keySet()) {
            sb.append("\t").append(string).append(" : ").append(accuracy.get(string)).append("%").append("\n");
        }
        return sb.toString();
    }

    public ArrayList<FeatureVector> getVectorSet() {
        return vectorSet;
    }

    public double[] getMaxes() {
        return maxes;
    }

    public double[] getMins() {
        return mins;
    }

    public double[] getDiffs() {
        return diffs;
    }

    public int getFeatureVectorLength() {
        return featureVectorLength;
    }

    public HashMap<String, Double> getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(HashMap<String, Double> accuracy) {
        this.accuracy = accuracy;
    }

    public int getK() {
        return k;
    }

    public void setK(int k) {
        this.k = k;
        calculateAccuracy();
    }

}
