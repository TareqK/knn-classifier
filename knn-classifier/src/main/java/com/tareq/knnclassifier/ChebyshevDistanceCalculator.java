/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tareq.knnclassifier;

/**
 *
 * @author tareq
 */
public class ChebyshevDistanceCalculator implements DistanceCalculator {

    @Override
    public double calculate(double[] a, double[] b) {
        double distance = 0;
        for (int i = 0; i < a.length; i++) {
            double currentDistance = Math.abs(a[i] - b[i]);
            distance = (currentDistance > distance) ? currentDistance : distance;

        }
        return distance;
    }
}
