/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tareq.knnclassifier;

import java.util.Arrays;

/**
 *
 * @author tareq
 */
public class FeatureVector {
    
    public double[] features;
    public int index;
    public String classLabel;

    public FeatureVector(double[] features, int index, String classLabel) {
        this.features = features;
        this.index = index;
        this.classLabel = classLabel;
    }
    
    public FeatureVector(double[] features){
        this.features = features;
        this.index = 0;
        this.classLabel= null;
    }

    public double[] getFeatures() {
        return features;
    }

    public void setFeatures(double[] features) {
        this.features = features;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getClassLabel() {
        return classLabel;
    }

    public void setClassLabel(String classLabel) {
        this.classLabel = classLabel;
    }

    @Override
    public String toString() {
        return "FeatureVector{" + "features=" +Arrays.toString( features) + ", index=" + index + ", classLabel=" + classLabel + '}';
    }

}
