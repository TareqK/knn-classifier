/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tareq.knnclassifier;

import static java.lang.Math.abs;

/**
 *
 * sum_k(delta_ijk * d_ijk ) / sum_k( delta_ijk )
 *
 * z_ik = (r_ik - 1)/(max(r_ik) - 1)
 *
 * @author tareq
 */
public class GowerDistanceCalculator implements DistanceCalculator {

    private double[] weights;

    public GowerDistanceCalculator() {
        weights = null;
    }

    public GowerDistanceCalculator(double[] weights) {
        this.weights = weights;
    }

    @Override
    public double calculate(double[] a, double[] b) {
        if (weights == null || weights.length == 0) {
            seedWeights(a.length);
        }
        if (weights.length != a.length) {
            return -1;
        } else {
            double weightedSum = 0;
            double sumOfWeights = 0;
            for (int i = 0; i < a.length; i++) {
                sumOfWeights += weights[i];
                weightedSum += weights[i] * abs(a[i] - b[i]);//we already normalized the data, so we dont have to calculate r_k
            }
            return (weightedSum/sumOfWeights);
        }

    }

    private void seedWeights(int length) {
        weights = new double[length];
        for (int i = 0; i < weights.length; i++) {
            this.weights[i] = 1;
        }
    }

    public double[] getWeights() {
        return weights;
    }

    public void setWeights(double[] weights) {
        this.weights = weights;
    }

}
