/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tareq.knnclassifier;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;

/**
 *
 * @author tareq
 */
public class FileUtils {

    public static ArrayList<FeatureVector> readFile(File csvFile) {
        ArrayList<FeatureVector> featureVectors = new ArrayList();
        try (Reader in = new FileReader(csvFile)) {
            Iterable<CSVRecord> records = CSVFormat.DEFAULT
                    .withFirstRecordAsHeader()
                    .parse(in);
            int index = 0;
            for (CSVRecord record : records) {
                int size = record.size();
                double[] features = new double[size - 1];
                for (int i = 0; i < size - 1; i++) {
                    try {
                        features[i] = (Double.parseDouble(record.get(i)));
                    } catch (NumberFormatException ex) {
                        features[i] = 0;
                    }
                }
                featureVectors.add(new FeatureVector(features, index, record.get(size - 1)));
                index++;

            }

        } catch (IOException ex) {
            Logger.getLogger(FileUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
        return featureVectors;
    }

    public static void saveFile(ArrayList<FeatureVector> vectors, File file) {
        try {
            CSVPrinter printer = CSVFormat.DEFAULT
                    .print(file, Charset.defaultCharset());
            printer.println();
            for (FeatureVector v : vectors) {
                ArrayList<String> features = new ArrayList<>();
                for (int i = 0; i < v.getFeatures().length; i++) {
                    features.add(String.valueOf(v.getFeatures()[i]));
                }
                features.add(v.getClassLabel());
                printer.printRecord(features);
            }
            printer.flush();
        } catch (IOException ex) {
            Logger.getLogger(FileUtils.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
