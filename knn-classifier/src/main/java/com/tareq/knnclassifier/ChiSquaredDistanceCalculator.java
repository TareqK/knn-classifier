/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tareq.knnclassifier;

import static java.lang.Math.pow;
import java.util.List;

/**
 *
 * @author tareq
 */
public class ChiSquaredDistanceCalculator implements DistanceCalculator {


    @Override
    public double calculate(double[] a, double[] b) {
        if(a.length != b.length){
            return -1;
           }else{
           return new EuclideanDistanceCalculator().calculate(a, b)/new ManhattanDistanceCalculator().calculate(a, b)/2;
        }
        
    }
    
}
