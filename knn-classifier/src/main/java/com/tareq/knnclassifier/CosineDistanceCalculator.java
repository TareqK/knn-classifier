/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tareq.knnclassifier;

/**
 *
 * @author tareq
 */
public class CosineDistanceCalculator implements DistanceCalculator {

    @Override
    public double calculate(double[] a, double[] b) {
       return  dotProduct(a,b)/new EuclideanDistanceCalculator().calculate(a,b);
    }
  
    private double dotProduct(double[] a,double[] b){
        double sum =0;
        for(int i =0; i<a.length;i++){
            sum  += a[i]*b[i];
        }
        return sum;
    }
}
