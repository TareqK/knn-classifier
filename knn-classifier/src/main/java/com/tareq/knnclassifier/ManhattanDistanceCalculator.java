/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tareq.knnclassifier;

import static java.lang.Math.abs;

/**
 *
 * @author tareq
 */
public class ManhattanDistanceCalculator implements DistanceCalculator {

    @Override
    public double calculate(double[] a, double[] b) {
        double sum = 0;
        for(int i=0;i<a.length;i++){
            sum+=abs(a[i]-b[i]);
        }
        return sum;
    }
    
}
