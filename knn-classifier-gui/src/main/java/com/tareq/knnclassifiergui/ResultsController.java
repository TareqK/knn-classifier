/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tareq.knnclassifiergui;

import com.tareq.knnclassifier.FeatureVector;
import com.tareq.knnclassifier.FileUtils;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author tareq
 */
public class ResultsController implements Initializable {

    
    @FXML
    private Button dismissButton;
    
    @FXML
    private Button saveButton;
    
    @FXML
    private TableView<FeatureVector> resultsTable;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    @FXML
    private void  onDismissButtonAction(ActionEvent e){
       Stage stage = (Stage) this.dismissButton.getScene().getWindow();
       stage.close();
    }

    @FXML
    private void onSaveButtonAction(ActionEvent e){
          FileChooser chooser = new FileChooser();
        chooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("CSV files(*.csv)", "*.csv"));
        File file = chooser.showSaveDialog(dismissButton.getScene().getWindow());
        if (file != null) {
            ArrayList<FeatureVector> featureVectors =  new ArrayList<>();
            featureVectors.addAll(resultsTable.getItems());
            FileUtils.saveFile(featureVectors, file);
        }
         Stage stage = (Stage) this.dismissButton.getScene().getWindow();
       stage.close();
    }
    public Button getDismissButton() {
        return dismissButton;
    }

    public void setDismissButton(Button dismissButton) {
        this.dismissButton = dismissButton;
    }

    public TableView<FeatureVector> getResultsTable() {
        return resultsTable;
    }

    public void setResultsTable(TableView<FeatureVector> resultsTable) {
        this.resultsTable = resultsTable;
    }
    
    
    
}
