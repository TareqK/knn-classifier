package com.tareq.knnclassifiergui;

import com.tareq.knnclassifier.ChebyshevDistanceCalculator;
import com.tareq.knnclassifier.ChiSquaredDistanceCalculator;
import com.tareq.knnclassifier.CosineDistanceCalculator;
import com.tareq.knnclassifier.EuclideanDistanceCalculator;
import com.tareq.knnclassifier.FeatureVector;
import com.tareq.knnclassifier.FileUtils;
import com.tareq.knnclassifier.GowerDistanceCalculator;
import com.tareq.knnclassifier.KnnClassifier;
import com.tareq.knnclassifier.ManhattanDistanceCalculator;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;

public class MainController implements Initializable {

    private KnnClassifier classifier;

    @FXML
    private TextField weightsTextField;

    @FXML
    private TextArea statArea;

    @FXML
    private Button enterFeatureButton;

    @FXML
    private Button openFileButton;

    @FXML
    private TextField kTextField;

    @FXML
    private TableView<FeatureVector> vectorSetTable;

    @FXML
    private MenuItem openTrainingSetItem;

    @FXML
    private MenuItem quitItem;

    @FXML
    private CheckMenuItem manhattanMenuItem;

    @FXML
    private CheckMenuItem euclideanMenuItem;

    @FXML
    private CheckMenuItem chiSquaredMenuItem;

    @FXML
    private CheckMenuItem cosineMenuItem;

    @FXML
    private CheckMenuItem chebyshevMenuItem;

    @FXML
    private CheckMenuItem gowerMenuItem;

    @FXML
    private MenuItem aboutMenuItem;

    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }

    @FXML
    private void onAboutMenuItemAction(ActionEvent e) {

    }

    @FXML
    private void onEnterFeatureButtonAction(ActionEvent e) {

    }

    @FXML
    private void onOpenFileButtonAction(ActionEvent e) {
        FileChooser chooser = new FileChooser();
        chooser.getExtensionFilters().add(new ExtensionFilter("CSV files(*.csv)", "*.csv"));
        File file = chooser.showOpenDialog(statArea.getScene().getWindow());
        if (file != null) {
            ArrayList<FeatureVector> featureVectors = FileUtils.readFile(file);
            classify(featureVectors);
        }
    }
    
    @FXML 
    private void onKTextFieldAction(ActionEvent e){
        String kString = kTextField.getText();
        if(kString!=null &&  this.classifier!=null){
            try{
              this.classifier.setK(Integer.parseInt(kString));
              statArea.setText(this.classifier.getStats());
            }catch(NumberFormatException ex){
                
            }
        }
    }

    @FXML
    private void onWeightsTextFieldAction(ActionEvent e) {
        String[] weightStrings = weightsTextField.getText().split(",");
            if (this.classifier != null && weightStrings.length == this.classifier.getFeatureVectorLength()) {
                double[] weights = new double[this.classifier.getFeatureVectorLength()];
                for (int i = 0; i < this.classifier.getFeatureVectorLength(); i++) {
                    try {
                        weights[i] = Double.parseDouble(weightStrings[i]);
                    } catch (NumberFormatException ex) {
                        weights[i] = 1;
                    }
                }
                this.classifier.calculateGowerAccuracy(weights);
                statArea.setText(this.classifier.getStats());
            }
        
    }

    private void classify(ArrayList<FeatureVector> featureVectors) {
        try {
            if (manhattanMenuItem.isSelected()) {
                TableView<FeatureVector> results = new TableView<>();
                featureVectors.forEach(vector -> this.classifier.classify(vector, new ManhattanDistanceCalculator()));
                spawnResultScreen(featureVectors, "Manhattan Distance");
            }
            if (euclideanMenuItem.isSelected()) {
                featureVectors.forEach(vector -> this.classifier.classify(vector, new EuclideanDistanceCalculator()));
                spawnResultScreen(featureVectors, "Euclidean Distance");
            }
            if (chiSquaredMenuItem.isSelected()) {
                featureVectors.forEach(vector -> this.classifier.classify(vector, new ChiSquaredDistanceCalculator()));
                spawnResultScreen(featureVectors, "Chi Squared Distance");
            }
            if (cosineMenuItem.isSelected()) {
                featureVectors.forEach(vector -> this.classifier.classify(vector, new CosineDistanceCalculator()));
                spawnResultScreen(featureVectors, "Cosine Distance");
            }
            if (chebyshevMenuItem.isSelected()) {
                featureVectors.forEach(vector -> this.classifier.classify(vector, new ChebyshevDistanceCalculator()));
                spawnResultScreen(featureVectors, "Chebyshev Distance");
            }
            if (gowerMenuItem.isSelected()) {
                GowerDistanceCalculator gower;
                String[] weightStrings = weightsTextField.getText().split(",");
                if (this.classifier != null && weightStrings.length == this.classifier.getFeatureVectorLength()) {
                    double[] weights = new double[this.classifier.getFeatureVectorLength()];
                    for (int i = 0; i < this.classifier.getFeatureVectorLength(); i++) {
                        try {
                            weights[i] = Double.parseDouble(weightStrings[i]);
                        } catch (NumberFormatException ex) {
                            weights[i] = 1;
                        }
                    }
                    gower = new GowerDistanceCalculator(weights);
                } else {
                    gower = new GowerDistanceCalculator();
                }
                featureVectors.forEach(vector -> this.classifier.classify(vector, gower));
                spawnResultScreen(featureVectors, "Gower Distance");
            }

        } catch (NumberFormatException ex) {
        }
    }

    @FXML
    private void onOpenTrainingSetItemAction(ActionEvent e) {
        FileChooser chooser = new FileChooser();
        chooser.getExtensionFilters().add(new ExtensionFilter("CSV files(*.csv)", "*.csv"));
        File file = chooser.showOpenDialog(statArea.getScene().getWindow());
        if (file != null) {
            this.classifier = new KnnClassifier(file, Integer.parseInt(kTextField.getText()));
            statArea.setText(this.classifier.getStats());
            generateTable(this.vectorSetTable);
            vectorSetTable.getItems().addAll(this.classifier.getVectorSet());
        }
    }

    public void generateTable(TableView<FeatureVector> tableView) {
        TableColumn<FeatureVector, String> idColumn = new TableColumn();
        idColumn.setText("id");
        idColumn.setText("id");
        idColumn.setCellValueFactory(c -> new IdGenerator().getValue(c.getValue()));
        tableView.getColumns().add(idColumn);
        for (int i = 0; i < this.getClassifier().getFeatureVectorLength(); i++) {
            TableColumn<FeatureVector, String> column = new TableColumn<>();
            column.setText(String.valueOf(i));
            FeatureCellGenerator generator = new FeatureCellGenerator(i);
            column.setCellValueFactory(c -> generator.getValue(c.getValue()));
            tableView.getColumns().add(column);
        }
        TableColumn<FeatureVector, String> classColumn = new TableColumn();
        classColumn.setText("class");
        classColumn.setCellValueFactory(c -> new SimpleStringProperty(c.getValue().getClassLabel()));
        tableView.getColumns().add(classColumn);
    }

    @FXML
    private void onQuitItemAction(ActionEvent e) {
        System.exit(0);
    }

    public TextArea getStatArea() {
        return statArea;
    }

    public void setStatArea(TextArea statArea) {
        this.statArea = statArea;
    }

    public Button getEnterFeatureButton() {
        return enterFeatureButton;
    }

    public void setEnterFeatureButton(Button enterFeatureButton) {
        this.enterFeatureButton = enterFeatureButton;
    }

    public Button getEnterFileButton() {
        return openFileButton;
    }

    public void setEnterFileButton(Button openFileButton) {
        this.openFileButton = openFileButton;
    }

    public TextField getkTextField() {
        return kTextField;
    }

    public void setkTextField(TextField kTextField) {
        this.kTextField = kTextField;
    }

    public TableView getVectorSetTable() {
        return vectorSetTable;
    }

    public void setVectorSetTable(TableView vectorSetTable) {
        this.vectorSetTable = vectorSetTable;
    }

    public MenuItem getOpenTrainingSet() {
        return openTrainingSetItem;
    }

    public void setOpenTrainingSet(MenuItem openTrainingSetItem) {
        this.openTrainingSetItem = openTrainingSetItem;
    }

    public MenuItem getQuit() {
        return quitItem;
    }

    public void setQuit(MenuItem quitItem) {
        this.quitItem = quitItem;
    }

    public CheckMenuItem getManhattanMenuItem() {
        return manhattanMenuItem;
    }

    public void setManhattanMenuItem(CheckMenuItem manhattanMenuItem) {
        this.manhattanMenuItem = manhattanMenuItem;
    }

    public CheckMenuItem getEuclideanMenuItem() {
        return euclideanMenuItem;
    }

    public void setEuclideanMenuItem(CheckMenuItem euclideanMenuItem) {
        this.euclideanMenuItem = euclideanMenuItem;
    }

    public CheckMenuItem getChiSquaredMenuItem() {
        return chiSquaredMenuItem;
    }

    public void setChiSquaredMenuItem(CheckMenuItem chiSquaredMenuItem) {
        this.chiSquaredMenuItem = chiSquaredMenuItem;
    }

    public CheckMenuItem getCosineMenuItem() {
        return cosineMenuItem;
    }

    public void setCosineMenuItem(CheckMenuItem cosineMenuItem) {
        this.cosineMenuItem = cosineMenuItem;
    }

    public CheckMenuItem getChebyshevMenuItem() {
        return chebyshevMenuItem;
    }

    public void setChebyshevMenuItem(CheckMenuItem chebyshevMenuItem) {
        this.chebyshevMenuItem = chebyshevMenuItem;
    }

    public CheckMenuItem getGowerMenuItem() {
        return gowerMenuItem;
    }

    public void setGowerMenuItem(CheckMenuItem gowerMenuItem) {
        this.gowerMenuItem = gowerMenuItem;
    }

    public MenuItem getAboutMenuItem() {
        return aboutMenuItem;
    }

    public void setAboutMenuItem(MenuItem aboutMenuItem) {
        this.aboutMenuItem = aboutMenuItem;
    }

    public KnnClassifier getClassifier() {
        return classifier;
    }

    public void setClassifier(KnnClassifier classifier) {
        this.classifier = classifier;
    }

    private static class FeatureCellGenerator {

        private final int featureId;

        public FeatureCellGenerator(int featureId) {
            this.featureId = featureId;
        }

        public SimpleStringProperty getValue(FeatureVector vector) {
            return new SimpleStringProperty(String.valueOf(vector.getFeatures()[featureId]));
        }
    }

    private static class IdGenerator {

        public SimpleStringProperty getValue(FeatureVector vector) {
            return new SimpleStringProperty(String.valueOf(vector.getIndex()));
        }
    }

    private void spawnResultScreen(ArrayList<FeatureVector> resultVectors, String classifier) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/fxml/Results.fxml"));
            ResultsController controller = new ResultsController();
            fxmlLoader.setController(controller);

            Scene scene = new Scene(fxmlLoader.load(), 600, 400);
            Stage stage = new Stage();
            stage.setTitle("Results Window for ".concat(classifier));
            stage.setScene(scene);
            generateTable(controller.getResultsTable());
            controller.getResultsTable().getItems().addAll(resultVectors);

            stage.showAndWait();
        } catch (IOException ex) {
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
